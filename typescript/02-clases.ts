class Persona {
    nombre: string;
    apellido: string;
    edad: number;

    saludar(): void {
        console.log('Hola mi nombre es: ' + this.nombre + ' ' + this.apellido);
    }
}

var estudiante: Persona = new Persona();
estudiante.nombre = 'Jose';
estudiante.apellido = 'Calderon';

estudiante.saludar();

// para compilar y ejecutar
// tsc 02-clases.ts
// node 02-clases.js