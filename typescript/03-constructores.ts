class Persona {
    nombre: string;
    apellido: string;
    edad: number;

    constructor(nombre: string, apellido: string, edad: number) {
        this.nombre = nombre;
        this.apellido = apellido
        this.edad = edad;
    }

    saludar(): void {
        console.log('Hola mi nombre es: ' + this.nombre + ' ' + this.apellido);
    }
}

var estudiante: Persona = new Persona('Jose', 'Calderon', 23);

estudiante.saludar();

// para compilar y ejecutar
// tsc 03-constructores.ts
// node 03-constructores.js