/*
 Tres elementos principales (no son los únicos)

** Componentes **
    * Los Componentes controlan secciones de nuestro HTML, nuestas vistas.
    * El Componente es una clase, esta clase va a tener propiedades (la información de la vista)
      y métodos (el comportamiento) que nos van a permitir modificar la vista.

    export class BuscarComponent {
        texto: string;
        buscar(texto: string) {
            // codigo
        }
    }

    * Para crear una aplicación en Angular vamos a necesitar por lo menos un Componente (raíz) pero
      generalmente vamos a tener más de uno.     

** Enrutamientos **
    * El enrutamient (o Routing) l outilizamos para mostrar diferentes areas de nuestra aplicación
      dependiendo de la dirección URL que el usuario utilice.

      http:**www.mi-aplicacion.com/ -> inicio
      /nosotros -> pagina 'nosotros'

    * Nos van a servir para:
        - Compartir  la dirección URL
        - Hacer bookmarks
        - Mantener la página en la que estamos si hacemos un refresh

** Directivas **
    * Las Directivas nos sirven para hacer cambios en el DOM de nuestras páginas.
    * Existen Directivas de fábrica como *ngFor y *ngIF.
    * Podemos crear nuestras propias Directivas.
    * Para definirlas utilizamos el decorador @Directive y le pasamos Metadata (un objeto de configuracion).
    * Son de dos tipos: Estructural y Atributo.
    * Las Directivas Estructurales se utilizan para crear, remover y/o reemplazar elementos del DOM (*ngIf, *ngFor, ngSwitch)
    * Las Directivas de Atributo las utilizamos para cambiar la apariencia o comportamiento de un elemento en el DOM que ya existe.
      En las plantillas se ven como atributos HTML (ngStyle, ngClass, ngModel)
*/