// let permite de definir variables cuyo rango de actuacion es el bloque donde se haya definido

var i = 0;

for(let i=0; i<5; i++) {
    console.log(`-- ${i}`);
}

console.log(i);
