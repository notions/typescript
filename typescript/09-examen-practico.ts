
// Uso de Let y Const
// var nombre = "Ricardo Tapia";
// var edad = 23;

// var PERSONAJE = {
//   nombre: nombre,
//   edad: edad
// };

function SOLUCION1() {
    let nombre: string = "Ricardo Tapia";
    let edad: number = 23;
  
    let PERSONAJE = {
      nombre: nombre,
      edad: edad,
    }
  }
  
  
  // Cree una interfaz que sirva para validar el siguiente objeto
  // var batman = {
  //   nombre: "Bruno Díaz",
  //   artesMarciales: ["Karate","Aikido","Wing Chun","Jiu-Jitsu"]
  // }
  
  function SOLUCION2() {
    interface superHero {
      nombre: string,
      artesMarciales: Array<string>,
    }
  
    let batman: superHero  = {
      nombre: "Bruno Díaz",
      artesMarciales: ["Karate","Aikido","Wing Chun","Jiu-Jitsu"],
    }
  }
  
  // Convertir esta funcion a una funcion de flecha
  // function resultadoDoble( a, b ){
  //   return (a + b) * 2
  // }
  
  function SOLUCION3() {
    let resultadoDoble = ( a: number, b: number) => {
      return (a + b) * 2;
    }
  }
  
  // Función con parametros obligatorios, opcionales y por defecto
  // donde NOMBRE = obligatorio
  //       PODER  = opcional
  //       ARMA   = por defecto = "arco"
  // function getAvenger( nombre, poder, arma ){
  //   var mensaje;
  //   if( poder ){
  //      mensaje = nombre + " tiene el poder de: " + poder + " y un arma: " + arma;
  //   }else{
  //      mensaje = nombre + " tiene un " + poder
  //   }
  // };
  
  function SOLUCION4() {
    function getAvenger( nombre: string, poder?: string, arma: string = "arco" ) {
      let mensaje: string;
      if ( poder ) {
        mensaje = `${nombre} tiene el poder de: ${poder} y un arma: ${arma}`;
      } else {
        mensaje = `${nombre} tiene un ${poder}`;
      }
    }
  }
  
  // Cree una clase que permita manejar la siguiente estructura
  // La clase se debe de llamar rectangulo,
  // debe de tener dos propiedades:
  //   * base
  //   * altura
  // También un método que calcule el área  =  base * altura,
  // ese método debe de retornar un numero.
  
  function SOLUCION5() {
    class Rectangulo {
      base: number;
      altura: number;
  
      constructor( base: number, altura: number ) {
        this.base = base;
        this.altura = altura;
      }
  
      get getAltura() {
        return this.altura;
      }
  
      get getBase() {
        return this.base;
      }
  
      get getArea() {
        return this.base * this.altura;
      }
  
      imprimirArea() {
        return `El área del cuadrado es ${this.base * this.altura}`;
      }
    }
  }
  