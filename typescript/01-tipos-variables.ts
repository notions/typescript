// Tipos de variables en typescript

// string
var nombre: string = "Felipe";

// number
var edad: number = 10;

// boolean
var casado: boolean = false;

// Array - Una forma de declaracion
var amigos: Array<string> = ['Carlos', 'Jose', 'Andres'];

// Array - Otra forma de declaracion
var hermanos: string[] = ['Ricardo', 'Luis'];

// enums
enum Parentesco {Padre, Madre, Hermano, Hermana, Hijo, Hija};
var parentesto: Parentesco = Parentesco.Padre;

// any
var miVariable: any;
miVariable = 10;
miVariable = 'test';
miVariable = [1,2,3,4];

// void
function myFunction(parameter: string): void {
    // myFunction code
}

